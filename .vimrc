"Mark Micchell's vimrc

"pathogen for plugins
execute pathogen#infect()

"setting standard options
set nocompatible
set backspace=2
filetype plugin indent on
syntax on
set number
set showmode
set showcmd
set ruler
set incsearch
set hlsearch
colorscheme desert

set wildmode=longest,list
set pastetoggle=<f5>
nnoremap <f6> :set list!<cr>
nnoremap <f4> :set spell!<cr>

"use four-character tabs...
set tabstop=4 noexpandtab shiftwidth=4 softtabstop=4
"...except for the following filetypes
autocmd filetype python set tabstop=4 expandtab shiftwidth=4 softtabstop=4
autocmd filetype c set tabstop=8 noexpandtab shiftwidth=8 softtabstop=8
autocmd filetype html set tabstop=2 expandtab shiftwidth=2 softtabstop=2
autocmd filetype javascript set tabstop=2 expandtab shiftwidth=2 softtabstop=2

"let space be leader, backslash be localleader
let mapleader = " "
let maplocalleader = "\\"

"let jj take you out of insert mode
inoremap jj <Esc>

"let <leader><leader> take you out of visual mode
vnoremap <leader><leader> <Esc>

"faster, better autocomplete
inoremap jk <C-p>
inoremap kj <C-n>

"give Y consistent behavior
noremap Y y$

"let K be the opposite of J (i.e. a line break)
nnoremap K i<CR><Esc>k:s/\s*$//<CR>:nohl<CR>

"let leader =, -, and ~ create lines
nnoremap <leader>= yypVr=
nnoremap <leader>- yypVr-
nnoremap <leader>~ yypVr~

"let enter add a space below, and backspace add a space above
nnoremap <leader><CR> o<Esc>
nnoremap <leader><BS> O<Esc>

"add nohlsearch to C-l functionality
nnoremap <silent> <C-l> :<C-u>nohlsearch<CR><C-l>

"capitalize word
noremap <leader>c bgUl
noremap <leader>C guiwgUl

"let leader d make Python/Javascript-esque object
vnoremap <leader>d y<Esc>`>a"<Esc>`<i"<Esc>f"a: <Esc>pa,<Esc>

"because I'm sick of :tabe (yes, the trailing whitespace is deliberate)
nnoremap <leader>t :<C-u>tabedit 

"let leader e<something> open frequently edited files
noremap <leader>ev :tabedit $MYVIMRC<CR>G
noremap <leader>es :source $MYVIMRC<CR>
noremap <leader>eb :tabedit $HOME/.bash_profile<CR>G
noremap <leader>eg :tabedit $HOME/.gitconfig<CR>G

"programming-language specific comments
autocmd filetype perl   nnoremap <buffer> <localleader>c mmi#<Esc>`ml
autocmd filetype ruby   nnoremap <buffer> <localleader>c mmi#<Esc>`ml
autocmd filetype python nnoremap <buffer> <localleader>c mmi#<Esc>`ml
autocmd filetype javascript nnoremap <buffer> <localleader>c mmi//<Esc>`mll
autocmd filetype java       nnoremap <buffer> <localleader>c mmi//<Esc>`mll
autocmd filetype java vnoremap <buffer> <localleader>c <Esc>`>a*/<Esc>`<i/*<Esc>`>lll
autocmd filetype c    vnoremap <buffer> <localleader>c <Esc>`>a*/<Esc>`<i/*<Esc>`>lll
autocmd filetype html vnoremap <buffer> <localleader>c <Esc>`>a--><Esc>`<i<!--<Esc>`>llll

"my favorite spelling mistakes and shortcuts
abbr existance existence
abbr improt import
abbr pmain if __name__ == "__main__":
